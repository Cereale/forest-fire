# Forest fire model

https://en.wikipedia.org/wiki/Forest-fire_model

![presentation](demo.gif)

# Rules

- A burning cell turns into an empty cell
- A tree will burn if at least one neighbor is burning
- A tree ignites with probability f even if no neighbor is burning
- An empty space fills with a tree with probability p

# Run


```sh
python3 forest.py
```
