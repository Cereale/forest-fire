import pyxel
from random import random
# Note : The neighbour propagation accounts for all 8 surrounding cells

def neighbour_burning(grid, i, j):
    N = len(grid)
    output = False
    if ("fire" in
    [grid[(i+1)%N][(j)%N],
     grid[(i-1)%N][(j)%N],
     grid[(i)%N][(j-1)%N],
     grid[(i)%N][(j+1)%N]]):
        output = True
    return output

def neighbour_tree(grid, i, j):
    # If growth==True then neighbour tree is called.
    # Makes tree growing next to an existing tree more likely
    N = len(grid)
    output = False
    divider = 16
    C = [grid[(i+1)%N][(j)%N],
     grid[(i-1)%N][(j)%N],
     grid[(i)%N][(j-1)%N],
     grid[(i)%N][(j+1)%N]].count("tree")
    if (random() + C/divider >1):
        output = True
    return output

def grid_count(grid, string):
    n = len(grid)
    c = 0
    for i in range(n):
        c += grid[i].count(string)
    return c

class App:
    def __init__(self, p=0.001, f=0.000005, growth=False):
        # Grid size
        self.N = 100
        pyxel.init(self.N, self.N, fps=30)
        # p = probability of a tree growing
        self.p = p
        # f = probability of a tree being struck by lighting and starting to burn
        self.f = f
        self.grid = []
        self.last_fire = 0;
        self.max_day = 0;
        self.neighbour_growth = growth
        for i in range(self.N):
            self.grid.append([])
            for j in range(self.N):
                self.grid[i].append("empty")
                if (i==j==self.N/2):
                    self.grid[i][j]="tree"
        pyxel.run(self.update, self.draw)

    def update(self):
        copy = []
        for i in range(self.N):
            copy.append([])
            for j in range(self.N):
                copy[i].append(self.grid[i][j])
        for i in range(self.N):
            for j in range(self.N):
                if (self.grid[i][j]=="fire"):
                    copy[i][j] = "empty"
                if (self.grid[i][j]=="tree" and (neighbour_burning(self.grid, i, j) or random()<self.f)):
                    copy[i][j] = "fire"

                if self.neighbour_growth:
                    if ((random()<self.p and self.grid[i][j]=="empty") or neighbour_tree(self.grid, i, j)):
                        copy[i][j] = "tree"
                else:
                    if ((random()<self.p and self.grid[i][j]=="empty")):
                        copy[i][j] = "tree"
        for i in range(self.N):
            for j in range(self.N):
                self.grid[i][j] = copy[i][j]

        self.max_day = max(self.max_day, self.last_fire)
        if (grid_count(self.grid, "fire")==0):
            self.last_fire += 1
        else:
            self.last_fire = 0

    def draw(self):
        print("Day :", pyxel.frame_count, " |   Days since last fire :", self.last_fire, " |  Maximum : ", self.max_day)
        pyxel.cls(0)
        for i in range(self.N):
            for j in range(self.N):
                if self.grid[i][j]=="fire":
                    pyxel.pset(i, j, 8)
                if self.grid[i][j]=="tree":
                    pyxel.pset(i, j, 3)

App(growth=False)
